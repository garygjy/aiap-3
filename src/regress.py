# regress.py
# this file serves to fit data provided to regression models
# metrics will be used to determine the effectiveness and accuracy of such models

from sklearn.linear_model import Lasso, LassoCV, Ridge, ElasticNet, ElasticNetCV
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as mse


import math

class Regression():

    def __init__(self):
        self.las = Lasso()
        self.ridge = Ridge()
        self.etr = ExtraTreesRegressor()

    def eval_model(self, x, y):

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

        self.las.fit(x_train, y_train)
        predict_train = self.las.predict(x_train)
        predict_test  = self.las.predict(x_test)

        print("Evaluation Results for Lasso: ")
        print("Best score using Lasso = %f" % self.las.score(x_test, y_test))
        print('RMSE on training data: ', math.sqrt(mse(y_train, predict_train)))
        print('RMSE on test data: ',  math.sqrt(mse(y_test, predict_test)))

        self.ridge.fit(x_train, y_train)
        predict_train = self.ridge.predict(x_train)
        predict_test  = self.ridge.predict(x_test)

        print("Evaluation Results for Ridge: ")
        print("Best score using Ridge = %f" % self.ridge.score(x_test, y_test))
        print('RMSE on training data: ', math.sqrt(mse(y_train, predict_train)))
        print('RMSE on test data: ',  math.sqrt(mse(y_test, predict_test)))

        self.etr.fit(x_train, y_train)
        predict_train = self.etr.predict(x_train)
        predict_test  = self.etr.predict(x_test)

        print("Evaluation Results for ExtraTreesRegressor: ")
        print("Best score using ExtraTreesRegressor = %f" % self.etr.score(x_test, y_test))
        print('RMSE on training data: ', math.sqrt(mse(y_train, predict_train)))
        print('RMSE on test data: ',  math.sqrt(mse(y_test, predict_test)))
