# process_data.py
# this file serves to fetch data from a local .db file,
# correct any missing/incorrect data values, clean up and prepare the data that is to be used for ML model training

import sqlite3
import pandas as pd
import numpy as np

class Process_Data():

    def __init__():
        pass

    def import_from_db():
        db = sqlite3.connect('data/news_popularity.db')
        arti_df = pd.read_sql_query("SELECT * from articles", db)
        desc_df = pd.read_sql_query("SELECT * from description", db)
        kw_df = pd.read_sql_query("SELECT * from keywords", db)

        arti_df.replace(to_replace='monday', value = '1', inplace=True)
        arti_df.replace(to_replace='tuesday', value = '2', inplace=True)
        arti_df.replace(to_replace='wednesday', value = '3', inplace=True)
        arti_df.replace(to_replace='thursday', value = '4', inplace=True)
        arti_df.replace(to_replace='friday', value = '5', inplace=True)
        arti_df.replace(to_replace='saturday', value = '6', inplace=True)
        arti_df.replace(to_replace='sunday', value = '7', inplace=True)
        arti_df = arti_df.astype({'weekday':'int64'})
        desc_df.replace(to_replace='entertainment', value='1', inplace=True)
        desc_df.replace(to_replace='business', value='2', inplace=True)
        desc_df.replace(to_replace='world', value='3', inplace=True)
        desc_df.replace(to_replace='technology', value='4', inplace=True)
        desc_df.replace(to_replace='lifestyle', value='5', inplace=True)
        desc_df.replace(to_replace='social_media', value='6', inplace=True)
        desc_df['data_channel'] = desc_df['data_channel'].fillna('7')
        desc_df = desc_df.astype({'data_channel':'int64'})

        hrefs_index = desc_df['num_hrefs'].index[desc_df['num_hrefs'].apply(np.isnan)]
        desc_df.drop(hrefs_index,inplace=True)
        temp_df = pd.merge(arti_df, desc_df, on='ID', how='inner')
        valid_df = pd.merge(temp_df,  kw_df, on='ID', how='inner')

        # simple reordering to have target column as the last column
        cols = list(valid_df.columns.values)
        cols.remove('shares')
        cols.append('shares')
        valid_df = valid_df[cols]
        final_df = valid_df.drop(columns=['url', 'ID'])

        #bins = [0, 1000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 850000]
        #final_df['shares_cat'] = pd.cut(final_df['shares'], bins=bins)

        labels = ["{0} - {1}".format(i, i + 49999) for i in range(0, 900000, 50000)]
        final_df['shares_cat'] = pd.cut(final_df['shares'], range(0, 950000, 50000), right=False, labels=labels)
        print(final_df.shares_cat.sort_values().unique())

        print(final_df)
        return final_df
