# main.py
# this file serves as the main ML pipeline
# using pre-processed data from process_data.py
# and the models from classify.py and regress.py to predict the number of shares(popularity) for a news article


import pandas

from sklearn import pipeline
from sklearn.impute import KNNImputer

from process_data import Process_Data as pd
from regress import Regression
from classify import Classification

class myPipeline():

    def __init__(self):
        self.import_data()
        self.impute_data()
        self.clean_up()
        pass

    def import_data(self):
        print("Fetching data from database")
        self.data = pd.import_from_db()
        self.x = self.data.drop(columns=['shares', 'shares_cat'])
        self.y_num = self.data['shares']
        self.y_cat = self.data['shares_cat']

    def impute_data(self):
        # impute data with all possible columns to ensure best possible accuracy for imputed data
        print("Imputing missing data")
        imputer = KNNImputer(n_neighbors=20, weights='distance')
        imputer.fit(self.x, self.y_num)
        x_trans = imputer.transform(self.x)
        column_names = ['timedelta','weekday','n_tokens_title','n_tokens_content','n_unique_tokens',
                        'n_non_stop_words','n_non_stop_unique_tokens','num_hrefs','num_self_hrefs','num_imgs',
                        'num_videos','n_comments','average_token_length','data_channel','self_reference_min_shares',
                        'self_reference_max_shares','self_reference_avg_shares','num_keywords','kw_min_min','kw_max_min',
                        'kw_avg_min','kw_min_max','kw_max_max','kw_avg_max','kw_min_avg','kw_max_avg','kw_avg_avg']
        self.x = pandas.DataFrame.from_records(x_trans, columns=column_names)

    def clean_up(self):
        # drop columns that are detailed statistics on target value
        self.x.drop(columns=['self_reference_min_shares','self_reference_max_shares',
                             'self_reference_avg_shares', 'kw_min_min','kw_max_min','kw_avg_min',
                             'kw_min_max','kw_max_max','kw_avg_max',
                             'kw_min_avg','kw_max_avg','kw_avg_avg'], inplace=True)


    def main(self):
        print("Ready to evaluate")
        while True:
            print("Please choose the approach")
            print("Enter 1 for regression, 2 for classification, 3 to end")
            print("press Ctrl-D at anytime to exit")
            temp = input()
            if len(temp.split()) >= 2:
                print("Too many inputs given. Please try again")
                continue
            if temp == '1':
                print("Selected Regression Approach")
                regress = Regression()
                regress.eval_model(self.x, self.y_num)

            elif temp == '2':
                print("Selected Classification Approach")
                classify = Classification()
                classify.eval_model(self.x, self.y_cat)

            elif temp == '3':
                break
            else:
                print("Invalid input. Please try again")


if __name__ == '__main__':
    ml_pipeline = myPipeline()
    ml_pipeline.main()
