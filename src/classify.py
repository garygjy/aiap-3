# classify.py
# this file serves to fit data provided to classification models
# metrics will be used to determine the effectiveness and accuracy of such models

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

class Classification():

    def __init__(self):
        self.etc = ExtraTreesClassifier()
        self.svc = SVC(kernel='rbf')
        self.knc = KNeighborsClassifier()

    def eval_model(self, x, y):

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

        self.etc.fit(x_train, y_train)

        predict_train = self.etc.predict(x_train)
        predict_test  = self.etc.predict(x_test)

        print("Evaluation Results for ExtraTreesClassifier: ")
        print('Accuracy score on training data: ', accuracy_score(y_train, predict_train))
        print('Accuracy score on test data: ',  accuracy_score(y_test, predict_test))

        self.svc.fit(x_train, y_train)

        predict_train = self.svc.predict(x_train)
        predict_test  = self.svc.predict(x_test)

        print("Evaluation Results for SVC(kernel='rbf'): ")
        print('Accuracy score on training data: ', accuracy_score(y_train, predict_train))
        print('Accuracy score on test data: ',  accuracy_score(y_test, predict_test))

        self.knc.fit(x_train, y_train)

        predict_train = self.knc.predict(x_train)
        predict_test  = self.knc.predict(x_test)

        print("Evaluation Results for KNeighborsClassifier: ")
        print('Accuracy score on training data: ', accuracy_score(y_train, predict_train))
        print('Accuracy score on test data: ',  accuracy_score(y_test, predict_test))
