# Machine Learning Pipeline

### Key findings from exploratory Data Analysis

- There are 793 rows which have missing values under several columns in the description table pertaining to the number of links in the articles

- Both the article table and the description table have 39644 rows of data and there is a difference of 3964 rows between the keywords table and the article/description table, meaning there are 3964 articles that do not have any data or statistics on how the keywords in the article affects the number of shares.

- The full dataset with all tables merged and NaN values populated in replacement of the missing values has 21813 rows containing missing values, with the _num_videos_ variable having approximately half of its data missing at 18633 NaN values.

- The _n_comments_ variable is moderately positively correlated to the target variable _shares_ according to the Pearson's correlation coefficient and is thought to be the most important feature from the observations of using the feature selection from the scikit-learn library.


### Pipeline Breakdown

- Step 1: Fetching and processing of data _( module_name: process_data.py )_

  - The first step of the pipeline is to fetch the data from the url and do some preprocessing to ensure there are no missing values in the data obtained
  - Preprocessing was done to replace categorical values with numerical representatives.
  - Columns that are statistical data of the target variable are removed, such as self reference shares count, and keywords shares counts.
  - Rows that have too many missing values are removed as imputation of data for these rows with the KNNImputer will result in inaccurate imputation due to the lack of reference data in the row.
  - Target variable _shares_ has been binned into ranges of 50000, for facilitation of the classifiers.


- Step 2: Imputation of data _( module_name:  impute_data > main.py)_

  - The second step of the pipeline is the imputation of data, where missing values are infered from other values.
  - Data to be imputed are the number of videos and images for the articles.
  - The KNNImputer is used to better infer values from neighboring data points as the articles of similar content and data channel addressing the same topic, will likely be similar to each other and have similar number of videos and images.


- Step 3: Evaluation of models

  - The user can now enter values to choose which sets of models to evaluate. More information can be found in the `Executing the Pipeline` Section

### Executing the pipeline
1. Upon running _'run.sh'_, the pipeline will initialize and begin retrieving the   dataset from the .db file. Following that, preprocessing work will begin to replace categorical values with its numerical representatives. Unnecessary rows and columns will be removed. Imputation of data will begin to replace NaN / null values in the features columns.

2. Once the fetching of dataset and preprocessing of data is complete, user may enter 1, 2 or 3 to proceed to the next step

| Input 	|  Desc.                                            |
|-------	|---------------------------------------------------|
| 1     	| Selects the regression models for evaluation      |
| 2     	| Selects the classification models for evaluation  |
| 3    	  | Stops the program                                 |


  Once inputs are entered, the program will proceed to evaluate the selected set of models and print the values of relevant metrics used


### Choice of Models

The dataset consists of 34959 rows of data.
It has been determined through exploratory data analysis that 11 features will be used to fit the model.
Following the path in the flowchart provided at https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html, we arrive at the following models for regression and classification.

Regressors used: Lasso, Ridge and ExtraTreesRegressor

With the results of feature selection pointing towards _n_comments_ as an overwhelmingly important feature, the Lasso Model has been chosen as one of the models, since the Lasso model prefers solutions with fewer non-zero coefficients.

Ridge is selected as a comparison to Lasso as the tree-based feature selection under the exploratory data analysis does favor quite a number of other variables with less importance.

ExtraTreesRegressor is chosen to reduce variance as compared to the other ensemble models in hopes of yielding the better model.

Classifiers used: ExtraTreesClassifier, SVC(kernel='rbf') and KNeighborsClassifier

ExtraTreesClassifier is chosen with the same reason as its regressor counterpart, to reduce variance as compared to the other ensemble models in hopes of yielding the better model.

KNeighborsClassifier is chosen as the similarity of the articles will generally lead to a similar number of shares.

SVC is chosen as it is robust and can be optimised to handle different sample-target relationships based on the kernel used.


### Evaluation of Regressor Models
Making use of the score of each model and mean_squared_error metric in the _scikit-learn_ library, the Root Mean Squared Error (RMSE) of the regression models are, rounded to 6 _s.f._, as follows:

| Model             | RMSE (train) | RMSE (test)   |
|-------------------|--------------|---------------|
| Lasso   	        | 10795.0      | 7571.21       |
| Ridge             | 10794.8      | 7571.22       |
| ETR  	            | 0.0146471    | 8158.46       |

Looking at the RMSE for the training sat, the best model will be ExtraTreesRegressor with a RMSE of 0.0146471. Lasso and Ridge has a RMSE of 10795.0 and 10794.8 respectively. However looking at the RMSE for the test set, ExtraTreesRegressor has a RMSE of 8158.46, higher than that of Lasso and Ridge at 7571.21 and 7571.22 respectively.

The difference in RMSE between ExtraTreesRegressor and the other 2 models for the training set is huge at over 10000, whereas for the test set, the difference for the test set is approximately 600. This would mean that in this scernario, ExtraTreesRegressor is the better model.


### Evaluation of Classifier Models
Making use of the accuracy_score metric in the _scikit-learn_ library, the accuracy score of the classification models are, rounded to 5 _s.f._, as follows:

| Model 	          | Accuracy score (train) | Accuracy score (test)   |
|-------------------|------------------------|-------------------------|
| ETC   	          | 1.0        	           | 0.99471                 |
| SVC(kernel='rbf') | 0.99557                | 0.99514                 |
| KNC  	            | 0.99571                | 0.99514                 |

Based on the metrics above, all 3 models achieved impressive results as seen in the accuracy scores for both the training set and the test set.

The ExtraTreesRegressor achieved 100% accuracy on the training set but fell to last place in accuracy on the test set. The difference in the accuracy score however is not large across the board, coming to about a difference of 0.00443 for the training set and 0.00043 for the test set between the best and worst scoring model.


### Final Evaluation

While the overall accuracy score of classifiers are higher than that of the regressors(i.e regressors have a RMSE of significant value), the use of bins as target for classifiers definitely provided a larger margin for error as compared to regressors. Furthermore, it is tough to accurately represent continuous values in bins. Therefore, when predicting continuous values, it is better to use regressors as classifiers requires target values to be binned


### Folder Structure and Outline

The folder structure is as follows:
```
├── src
│    ├── process_data.py
│    ├── classify.py
│    ├── regress.py
│    └── main.py
├── data
│    └── news_popularity.db
├── README.md
├── eda.ipynb
├── requirements.txt
└── run.sh
```


### Personal information

Name: Gary Goh Jiang Yu<br>
Email: ggjy03@gmail.com
